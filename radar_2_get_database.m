function imdb = radar_2_get_database(dataDir, labelFileName)
% Returns scan metadata labeled +1 for RAIN and -1 for NO-RAIN.

% labelPath = '/scratch3/arunirc/radar_project/radar_data/gulf-coast-data/labels_collated_2.csv';

imdb.imageDir = dataDir;
imdb.classes.name = {'no-rain', 'rain'};

labelPath = fullfile(dataDir, labelFileName);
fid = fopen(labelPath);
assert(fid>0);
labeldata = textscan(fid, strtrim(repmat('%s ', 1, 3)), 'Delimiter', ',', 'HeaderLines',1);

scanID = labeldata{1};
label = labeldata{2};
imgPath = labeldata{3};

% scan ID
scanID = cellfun(@(x)(str2num(x)), scanID, 'un',0);
scanID = cell2mat(scanID);

% labels -- "NO RAIN" is "accept", "RAIN" is "reject"
y = cellfun(@(x)(strcmp(x,'reject')), label, 'un', 0);
y = cell2mat(y);

% check for missing files and remove them from being in use
selector = cellfun(@(x)(any(exist(fullfile(imdb.imageDir, x)))), ...
                            imgPath, 'un', 0);
selector = cell2mat(selector);

% IMDB images
imdb.images.name = imgPath(selector);
imdb.images.id = 1:length(imdb.images.name);
imdb.images.scan_id = scanID(selector);
imdb.images.label = 2*y(selector) - 1; % rain +1, no rain -1

% make this compatible with the OS imdb
imdb.meta.classes = imdb.classes.name ;
imdb.meta.inUse = true(1,numel(imdb.meta.classes)) ;

 
% display stats
np = sum(imdb.images.label==1);
nn = sum(imdb.images.label~=1);
n = np + nn;
fprintf('\n\tTotal samples: \t %d', n);
fprintf('\n\tFiles missing: \t %d', n - sum(selector));
fprintf('\nRejected (rain): \t %d \t (%.4f)', np, np/n);
fprintf('\nAccepted (no rain): \t %d \t (%.4f)\n', nn, nn/n);


% No standard image splits are provided for this dataset
imdb.sets = {'train', 'val', 'test'};
imdb.images.set = zeros(1,length(imdb.images.id));
classLabels = unique(imdb.images.label);
for c = 1:length(classLabels), 
    isclass = find(imdb.images.label == classLabels(c));
    
    % split equally into train, val, test
    order = randperm(length(isclass));
    subsetSize = ceil(length(order)/3);
    train = isclass(order(1:subsetSize));
    val = isclass(order(subsetSize+1:2*subsetSize));
    test  = isclass(order(2*subsetSize+1:end));
    
    imdb.images.set(train) = 1;
    imdb.images.set(val) = 2;
    imdb.images.set(test) = 3;
end

