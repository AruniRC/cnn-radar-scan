function code = get_saliency_maps(net, im, varargin)
%GET_SALIENCY_MAPS Computes image saliency maps for a particular class
%   This function uses the OPTS.CLASSID to get a saliency map over the
%   input image for that particular class. It backprops the errors from the
%   classification layer to get gradients on the image itself. The
%   magnitude of these gradients form a saliency heatmap for the presence 
%   of that class (e.g. "rain") at all locations in the image.
%   The function returns a saliency map for each image as a 4-D array
%   or a cell-array of 3-D maps in CODE.

opts.batchSize = 8 ;
opts.regionBorder = 0.05;
opts.classID = [];
opts = vl_argparse(opts, varargin) ;
assert(isempty(opts.classID), 'opts.classID must not be empty');

if ~iscell(im)
  im = {im} ;
end

res = [] ;
cache = struct() ;
resetCache() ;

    % for each image
    function resetCache()
        cache.images = cell(1,opts.batchSize) ;
        cache.indexes = zeros(1, opts.batchSize) ;
        cache.numCached = 0 ;
    end

    function flushCache()
        if cache.numCached == 0, return ; end
        images = cat(4, cache.images{:}) ;
        images = bsxfun(@minus, images, net.normalization.averageImage) ;
        if net.useGpu
            images = gpuArray(images) ;
            one = gpuArray(single(1)) ;
        else
            one = single(1) ;
        end
        
        % backprop
        net.layers{end}.class = labels ;
        res = vl_simplenn(net, im, one, res, ...
          'conserveMemory', opts.conserveMemory, ...
          'sync', opts.sync) ;
        
                    
        for q=1:cache.numCached
            code{cache.indexes(q)} = code_(:,q) ;
        end
        resetCache() ;
    end

    function appendCache(i,im)
        cache.numCached = cache.numCached + 1 ;
        cache.images{cache.numCached} = im ;
        cache.indexes(cache.numCached) = i;
        if cache.numCached >= opts.batchSize
            flushCache() ;
        end
    end

    code = {} ;
    for k=1:numel(im)
        appendCache(k, getImage(opts, single(im{k}), net.normalization.imageSize(1)));
    end
    flushCache() ;
end

% -------------------------------------------------------------------------
function reg = getImage(opts, im, regionSize)
% -------------------------------------------------------------------------
    reg = imresize(im, [regionSize, regionSize], 'bicubic') ;
end


