function vis_error_images(resultPath, imdbPath, radarMetadataPath, outDir, varargin)
%VIS_ERROR_IMAGES Display the wrongly-classified images in a webpage.
%   
%   NOTE: All the text file outputs are opened in "append" mode. So if you
%   are trying to re-run the program, PLEASE make sure to delete all the
%   generated text files from a previous program run.

assert(any(exist(resultPath, 'file')), ...
        'Result mat-file not found. Please specify correct RESULTPATH.');
assert(any(exist(imdbPath, 'file')), ...
        'IMDB mat-file not found. Please specify correct IMDBPATH.');
assert(any(exist(radarMetadataPath, 'file')), ...
        'METADATA mat-file not found. Please specify correct RADARMETADATAPATH.');
setup;

result = load(resultPath);
imdb = load(imdbPath);
metadata = load(radarMetadataPath);

% extract scan file name from radar metadata
filenames = cellfun(@(x)(strsplit(x,'/')), metadata.filelist, 'un', 0);
filenames = cellfun(@(x)(x{end}), filenames, 'un', 0);

[~,preds] = max(result.scores, [], 1);
errorIdx = (imdb.images.label ~= preds);
test = (imdb.images.set==3);

%{
testScores = result.scores(:,test);
[~,testPreds] = max(testScores, [], 1);
testLabels = imdb.images.label(test);
%}

% list of prediction errors on test set
namelist = imdb.images.name(errorIdx & test); 
labels = imdb.images.label(errorIdx & test);
scores = result.scores(:, errorIdx & test);

% write error data to disk
imOutPath = fullfile(outDir, 'error_images');
vl_xmkdir(imOutPath);
vl_xmkdir(fullfile(outDir, 'false_pos'));
vl_xmkdir(fullfile(outDir, 'false_neg'));

fid = fopen(fullfile(outDir, 'error_scan_names.txt'), 'a');
f1 = fopen(fullfile(outDir, 'error_scan_names_false_pos.txt'), 'a');
f2 = fopen(fullfile(outDir, 'error_scan_names_false_neg.txt'), 'a');
f3 = fopen(fullfile(outDir, 'error_scan_images_false_pos.html'), 'a');
f4 = fopen(fullfile(outDir, 'error_scan_images_false_neg.html'), 'a');

% Write HTML headers
htmlHeader = '<html><head><title>Radar scan error analysis</title></head>';
% <img src=contROC-compare.png width=600 border=0>
fprintf(f3, '%s<body>\n<h2>False positives: gt="no rain", pred="rain"</h2><br/>', htmlHeader);
fprintf(f4, '%s<body>\n<h2>False negatives: gt="rain", pred="no rain"</h2><br/>', htmlHeader);


for ii = 1:numel(namelist)
    disp(ii);
    name = namelist{ii}; 
    svmScore = scores(:,ii);
    img = imread(fullfile(imdb.imageDir, name));
    [~, imageName,~] = fileparts(name);
    
    % get scanID from metadata
    tmp = strsplit(imageName, '_');
    scanName = strjoin(tmp(1:3), '_'); 
    pos = ismember(filenames, [scanName '.gz']); 
    scanID = metadata.scanID(pos);
    
    if ~exist(fullfile(imOutPath, [imageName '.jpg']), 'file')
        imwrite(img, fullfile(imOutPath, [imageName '.jpg']));
    end 
    
    gtLabel = labels(ii);
    
    % Link to scan webpage at radar.cs.umass.edu
    station = scanName(1:4);
    month = [scanName(5:8), '-', scanName(9:10)];
    webLink = sprintf('http://radar.cs.umass.edu/scanlabeler/#?dataset=6&station=%s&month=%s&scan=%d', ...
    					station, month, scanID); 
    
    
    if gtLabel == 1 % no-rain
        if ~exist(fullfile(outDir, 'false_pos',[imageName '.jpg']), 'file')
            imwrite(img, fullfile(outDir, 'false_pos', [imageName '.jpg']));
        end
        fprintf(f1, '%d %s %d\n', scanID, scanName, floor(gtLabel/2));
            
        % HTML
        fprintf(f3, 'ScanID: %d<br/> <a href=%s>link</a><br/> Name: %s <br/> SVM score(no-rain, rain): (%f, %f)<br/> <a href=%s><img src=%s height=200></a><br/><hr/><br/>\n', ...
                        scanID, webLink, scanName, svmScore(1), svmScore(2), ['false_pos/' imageName '.jpg'], ['false_pos/' imageName '.jpg']); 
    else
        if ~exist(fullfile(outDir, 'false_neg',[imageName '.jpg']), 'file')
            imwrite(img, fullfile(outDir, 'false_neg', [imageName '.jpg']));
        end
        fprintf(f2, '%d %s %d\n', scanID, scanName, floor(gtLabel/2));
            
        % HTML
        fprintf(f4, 'ScanID: %d<br/> <a href=%s>link</a><br/> Name: %s <br/> SVM score(no-rain, rain): (%f, %f)<br/>  <a href=%s><img src=%s height=200></a><br/><hr/><br/>\n', ...
                        scanID, webLink, scanName, svmScore(1), svmScore(2), ['false_neg/' imageName '.jpg'], ['false_neg/' imageName '.jpg']);
    end
    
    fprintf(fid, '%d %s %d\n', scanID, scanName, floor(gtLabel/2));
    
end

fprintf(f3, '\n</body>\n</html>');
fprintf(f4, '\n</body>\n</html>');


fclose(fid);
fclose(f1); fclose(f2); fclose(f3); fclose(f4);
