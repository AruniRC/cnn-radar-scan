% -------------------------------------------------------------------------
function [ gif ] = renderscan( path, callsign, mode, varargin )
% -------------------------------------------------------------------------
%RENDER_SCAN Copy of original RENDERSCAN, but supports multiple modalities
%such as radial velocity ('vr') and spectrum width ('sw') in addition to
%the default relfectivity ('dz').

%radar loading params
DEFAULT_COORDINATES     = 'cartesian'; % 'polar', 'cartesian'
DEFAULT_SWEEPS          = 1;           %a vector of length 1, 2, or 3

%radar alignment params
DEFAULT_AZRES           = 0.5;         %positive real
DEFAULT_RANGERES        = 250;         %positive real
DEFAULT_RANGEMAX        = 37500;       %positive real

%image dimension params
DEFAULT_DIM             = uint16(800); %positive int
DEFAULT_DZLIM           = [-5, 30];    %tuple of reals
DEFAULT_DZMAP           = gray(256);   %colormap

DEFAULT_WRITE_LOCATION  = [];

% parse inputs
parser = inputParser;
addRequired  (parser,'path',                                @(x) exist(x,'file') == 2);
addRequired  (parser,'callsign',                            @(x) validateattributes(x,{'char'},{'numel',4}));
addParamValue(parser,'coordinates', DEFAULT_COORDINATES,    @(x) any(validatestring(x,{'polar','cartesian'})));
addParamValue(parser,'sweeps',      DEFAULT_SWEEPS,         @(x) validateattributes(x,{'numeric'},{'nonempty','positive','integer'}));% && numel(x) <= 3);
addParamValue(parser,'azres',       DEFAULT_AZRES,          @(x) validateattributes(x,{'numeric'},{'>',0}));
addParamValue(parser,'rangeres',    DEFAULT_RANGERES,       @(x) validateattributes(x,{'numeric'},{'>',0}));
addParamValue(parser,'rangemax',    DEFAULT_RANGEMAX,       @(x) validateattributes(x,{'numeric'},{'>',0}));
addParamValue(parser,'dim',         DEFAULT_DIM,            @(x) validateattributes(x,{'numeric'},{'integer','>',0}));
addParamValue(parser,'dzlim',       DEFAULT_DZLIM,          @(x) validateattributes(x,{'numeric'},{'numel',2}));
addParamValue(parser,'dzmap',       DEFAULT_DZMAP,          @(x) iptcheckmap(x,'renderscan','dzmap'));
addParamValue(parser,'outpath',     DEFAULT_WRITE_LOCATION, @(x) isempty(x) || exist(fileparts(x), 'file') == 7);

parse(parser, path, callsign, varargin{:});

path        = parser.Results.path;
callsign    = parser.Results.callsign;
coordinates = parser.Results.coordinates;
sweeps      = parser.Results.sweeps;
azres       = parser.Results.azres;
rangeres    = parser.Results.rangeres;
rangemax    = parser.Results.rangemax;
dim         = parser.Results.dim;
dzlim       = parser.Results.dzlim;
dzmap       = parser.Results.dzmap;
outpath     = parser.Results.outpath;

% read radar

%construct rsl2mat param struct
rsl2mat_params = struct('cartesian', false);%, 'nsweeps', max(sweeps));

radar = rsl2mat(path, callsign, rsl2mat_params);
radar = align_scan(radar, azres, rangeres, rangemax);

% render image

%extract the sweeps as matrices
[data, range, az, ~] = radar2mat(radar, {mode});

%subset the selected sweeps
data = data{1}(:,:,sweeps);

%set NaN to min reflectivity
data(isnan(data)) = min(dzlim);

%allocate gif size
if strcmp(coordinates, 'cartesian')
    %use dim instead
    gif = zeros(dim, dim, size(data,3));
else
    gif = zeros(size(data));
end

%render each sweep into a grayscale gif
for isweep = 1:numel(sweeps)
    sweep = data(:,:,isweep);
    if strcmp(coordinates, 'cartesian')
        %convert to cartesian coordinates
        sweep = mat2cart(sweep, az, range, dim, max(range), 'nearest');
    end
    
    gif(:,:,isweep) = mat2ind(sweep, dzlim, dzmap);
end

%set NaNs in the gif to 0
gif(isnan(gif)) = 0;

%convert to integer type
gif = uint8(gif);

%write image to file (if an outpath exists)
if ~isempty(outpath)
	imwrite(gif, outpath);
end

