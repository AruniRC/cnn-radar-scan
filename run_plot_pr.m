% Generate comparative PR-curves for methods.
%   This requires all the results to have been computed and saved.
%   Please change local paths suitably.



%% RCNN-VD-ft

info = load('data/v2-full/radar-sweeps-seed-01/result-rcnnvdft.mat');
gt = load('data/v2-full/radar-sweeps-seed-01/imdb/imdb-seed-1.mat');
imdb = load('data/v2-full/radar-sweeps-seed-01/imdb/imdb-seed-1.mat');

gt = imdb.images.label;
gt = imdb.images.label(imdb.images.set==3); % 1xN
scores = info.scores(:,imdb.images.set==3); % 2xN

gt_1 = gt;
gt_1(gt_1~=1) = -1;

gt_2 = gt;
gt_2(gt_2~=2) = -1;

% precision-recall of "rain" classifier SVM
[recall, precision, prinfo] = vl_pr(gt_2, scores(2,:));
save('data/v2-full/radar-sweeps-seed-01/pr-result-rcnnvdft.mat', ...
    'recall', 'precision', 'prinfo');


plot(recall, precision, 'r', 'LineWidth', 1)
hold on


%% RCNN-VD

info = load('data/v2-full/radar-sweeps-seed-01/result-rcnnvd.mat');
scores = info.scores(:,imdb.images.set==3); % 2xN

% precision-recall of "rain" classifier SVM
[recall, precision, prinfo] = vl_pr(gt_2, scores(2,:));
save('data/v2-full/radar-sweeps-seed-01/pr-result-rcnnvd.mat', ...
    'recall', 'precision', 'prinfo');

plot(recall, precision, 'b', 'LineWidth', 1)


%% DSIFT-2

info = load('data/v2-full/radar-sweeps-seed-01/result-dsift2.mat');
scores = info.scores(:,imdb.images.set==3); % 2xN

% precision-recall of "rain" classifier SVM
[recall, precision, prinfo] = vl_pr(gt_2, scores(2,:));
save('data/v2-full/radar-sweeps-seed-01/pr-result-dsift2.mat', ...
    'recall', 'precision', 'prinfo');

plot(recall, precision, 'k', 'LineWidth', 1)


%% DSIFT

info = load('data/v2-full/radar-sweeps-seed-01/result-dsift.mat');
scores = info.scores(:,imdb.images.set==3); % 2xN

% precision-recall of "rain" classifier SVM
[recall, precision, prinfo] = vl_pr(gt_2, scores(2,:));
save('data/v2-full/radar-sweeps-seed-01/pr-result-dsift.mat', ...
    'recall', 'precision', 'prinfo');

plot(recall, precision, 'g', 'LineWidth', 1)


%% plot labels
xlabel('recall');
ylabel('precision');


h = gcf;
set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])

print(h,'data/v2-full/radar-sweeps-seed-01/pr_comp','-dpdf','-r0')






%{
%% RCNN-ft

info = load('data/v2-full/radar-sweeps-seed-01/result-rcnn-ft.mat');
scores = info.scores(:,imdb.images.set==3); % 2xN

% precision-recall of "rain" classifier SVM
[recall, precision, prinfo] = vl_pr(gt_2, scores(2,:));

plot(recall, precision, 'g', 'LineWidth', 1)


%% RCNN

info = load('data/v2-full/radar-sweeps-seed-01/result-rcnn.mat');
scores = info.scores(:,imdb.images.set==3); % 2xN

% precision-recall of "rain" classifier SVM
[recall, precision, prinfo] = vl_pr(gt_2, scores(2,:));

plot(recall, precision, 'c', 'LineWidth', 1)

%}
