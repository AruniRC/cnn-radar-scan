% Script to debug a trial run of getting "rain" saliency map 

imdb = load('/scratch3/arunirc/radar_project/cnn-radar-scan/deep-face/data/v2-full/radar-sweeps-seed-01/imdb/imdb-seed-1.mat');
net = load('/scratch3/arunirc/radar_project/cnn-radar-scan/deep-face/data/models/ft-sweeps-full-vgg-m.mat');
numSample = 50;
outDir = '/scratch3/arunirc/radar_project/cnn-radar-scan/deep-face/data/v2-full/radar-sweeps-seed-01/vis-segment/saliency-maps';

rng(0);
setup;
vl_xmkdir(outDir);

switch(net.layers{end}.type)
    case 'softmax'
        net.layers{end} = struct('type', 'softmaxloss') ;
    case 'sofmaxloss'
        % already softmaxloss, do nothing.
    otherwise 
        error('Network final layer needs to be softmax loss');
end
res = [];
rainImgPos = find(imdb.images.label ==2);
rainImgPosSample = rainImgPos(randperm(length(rainImgPos), numSample));

for i = 1:numSample
    
    img = imread(fullfile(imdb.imageDir, ...
                    imdb.images.name{rainImgPosSample(i)}));
    imgOrig = img;
    img = imresize(img, net.normalization.imageSize(1:2), 'bicubic') ;
    img = single(img);
    img = bsxfun(@minus, img, net.normalization.averageImage) ;
    labels = 2;

    % back-prop
    one = single(1) ;
    net.layers{end}.class = labels ;
    res = vl_simplenn(net, img, one, res, ...
      'conserveMemory', true, ...
      'sync', true) ;

    % saliency map
    salImg = abs(squeeze(gather(res(1).dzdx)));
    salImg = max(salImg, [], 3);

    % distribution of saliency values
    [counts,edges] = histcounts(salImg(:));
    thresh = quantile(salImg(:),0.95) ; % 95% quantile

    % thresholded image
    threshImg = zeros(size(salImg), 'single');
    threshImg(salImg > thresh) = 1;


    % plot figures
    figH = figure(1);
    subplot(1,4,1); 
    imshow(imgOrig);
    title('Radar image');

    subplot(1,4,2);
    imagesc(salImg(:,:,1)); colormap hot; axis square; axis off;
    title('Saliency map');

    subplot(1,4,3);
    bar(edges(1:end-1), counts, 'b');
    hold on;
    plot([thresh,thresh], [0,max(counts)], 'r-');
    xlabel(sprintf('%f',thresh));
    title('Saliency distribution');
    axis square
    hold off

    subplot(1,4,4);
    imagesc(threshImg); colormap hot; axis square; axis off;
    title('Thresholded map');

    set(figH,'Units','normalized');
    set(figH,'Position',[0 0 1 1]);
    
    plotName = sprintf('%d.png', i);
    print(fullfile(outDir,plotName),'-dpng');

end
