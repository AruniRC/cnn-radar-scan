function run_dual_pol_render()

%% Set paths for radar libraries
% setup;
% addpath(genpath(fullfile(RADAR_PATH, 'rclass2014')));
% addpath(genpath(fullfile(RADAR_PATH, 'rscancv')));
RADAR_PATH = '/scratch3/arunirc/radar_project/';
addpath(genpath(fullfile(RADAR_PATH, 'wsrlib-1', 'wsrlib'))); % newer version


%% Test on rendering a single image

%{

/scratch4/arunirc/Data/radar/dual_pole/KBGM/2015/04/01/KBGM20150401_000138_V06.gz

%}

%{


%}

path = '/scratch4/arunirc/Data/radar/dual_pole/KAMX/2014/01/01/KAMX20140101_111000_V04.gz';
callsign = 'KAMX';


% path = '/scratch4/arunirc/Data/radar/dual_pole/KBGM/2015/04/01/KBGM20150401_000138_V06.gz';
% callsign = 'KBGM';

mode = 'rh'; % 'rh'
sweep = 2;

[~, map] = renderscan(path, callsign, mode, 'sweeps', sweep);

% threshold RH at 0.95
rhThresh = zeros(size(map));
rhThresh(map > 0.95) = 1;

mode = 'dz'; % 'rh'
img = renderscan(path, callsign, mode, 'sweeps', sweep);

%%

subplot(1,3,1);
imagesc(img); title('DZ map'); axis square; axis off;

subplot(1,3,2);
imagesc(map); title('RH map'); axis square; axis off;

subplot(1,3,3);
imagesc(rhThresh); title('RH thresh'); axis square; axis off;

    



size(img);





% -------------------------------------------------------------------------
function imdb = get_imdb_rendered_scans(metadata, opts)
% ------------------------------------------------------------------------- 
names = {};
labels = [];
stations = {};
k = 0;

% render the scans and save to file
for i = 1:length(metadata.filelist)
    outDir = fullfile(opts.renderPath, metadata.station{i});
    vl_xmkdir(outDir);
    [a,b,c] = fileparts(metadata.filelist{i});
    swp = strjoin(strsplit(num2str(opts.sweep),' '), '_');
    if isequal(opts.mode, 'dz')
        imgName = [b '_' swp '.jpg'];
    else
        imgName = [b '_' num2str(opts.mode) '_' ...
                    swp '.jpg'];
    end
    outPath = fullfile(outDir, imgName);
    
    % doesn't overwrite if output file already exists
    if ~exist(outPath, 'file')
        try
            switch opts.mode
                case 'dz'
                    img = renderscan(metadata.filelist{i}, metadata.station{i}, ...
                                     'dz', 'sweeps', opts.sweep);
                case 'vr'
                    img = renderscan(metadata.filelist{i}, metadata.station{i}, ...
                                     'vr', 'sweeps', opts.sweep);
                case 'sw'
                    img = renderscan(metadata.filelist{i}, metadata.station{i}, ...
                                     'sw', 'sweeps', opts.sweep);
                otherwise
                    error('Incorrect option for opts.mode');
            end
            imwrite(img, outPath);
            fprintf('%d. Saving render to disk: %s\n', i, outPath);
        catch
            warning('Error in renderscan() while rendering %s\n', ...
                            metadata.filelist{i});
            continue;
        end      
    else
        fprintf('%d. File already exists on disk: %s\n', i, outPath);
    end
    names{end+1} = [metadata.station{i} '/' imgName];
    labels(end+1) = double(metadata.label(i));
    stations{end+1} = metadata.station{i};
end

% imdb structure
imdb.images.name = names;
labels(labels==0) = 2;  % make lables 1,2 from 0,1
imdb.images.label = labels;
imdb.images.station = stations;
imdb.images.id = 1:length(imdb.images.name);

imdb.imageDir = opts.renderPath;
imdb.classes.name = {'no-rain', 'rain'};
imdb.meta.classes = imdb.classes.name ;
imdb.meta.inUse = true(1,numel(imdb.meta.classes)) ;

% No standard image splits are provided for this dataset
imdb.sets = {'train', 'val', 'test'};
imdb.images.set = zeros(1,length(imdb.images.id));
for c = 1:length(imdb.classes.name), 
    isclass = find(imdb.images.label == c);
    
    % split equally into train, val, test
    order = randperm(length(isclass));
    subsetSize = ceil(length(order)/3);
    train = isclass(order(1:subsetSize));
    val = isclass(order(subsetSize+1:2*subsetSize));
    test  = isclass(order(2*subsetSize+1:end));
    
    imdb.images.set(train) = 1;
    imdb.images.set(val) = 2;
    imdb.images.set(test) = 3;
end


                                        
% -------------------------------------------------------------------------
function metadata = get_metadata(radarPath)
% ------------------------------------------------------------------------- 
                                       
b = dir(radarPath);
b = b(arrayfun(@(x) ~strcmp(x.name(1),'.'),b)); %remove hidden files

% folders - 'KBGM', 'KOKX', ...
folders = b(arrayfun(@(x) (x.isdir), b));

filelist = {};
scan_id = [];
station = {};

for ii = 1:numel(folders)
    b = dir(fullfile(radarPath,folders(ii).name));
    b = b(arrayfun(@(x) ~strcmp(x.name(1),'.'),b));
    subfolders = b(arrayfun(@(x) (x.isdir), b));
    
    for jj = 1:numel(subfolders)
        subfolderPath = fullfile(radarPath,folders(ii).name, subfolders(jj).name);         
        fid = fopen(fullfile(subfolderPath, 'meta.csv'), 'r');
        meta = textscan(fid, strtrim(repmat('%s ', 1, 11)), ...
                                'Delimiter', ',', 'HeaderLines', 1);
        
        a = cellfun(@(x)(fullfile(subfolderPath, x)), meta{9}, 'un', 0);                    
        filelist = vertcat(filelist, a);             
        station = vertcat(station, meta{2});       
        b = cellfun(@str2num, meta{1}, 'un', 0);
        scan_id = vertcat(scan_id, cell2mat(b));
    end
end  
metadata.filelist = filelist;
metadata.scanID = scan_id;
metadata.station = station;
                                        
                                        
                                        
% -------------------------------------------------------------------------
function labels = get_scan_labels(scanID, metadata)
% -------------------------------------------------------------------------
if iscell(scanID), scanID = cell2mat(scanID); end

labeledScans = (cellfun(@(x)(str2double(x)), metadata{1}, 'un', 0))';
labeledScans = cell2mat(labeledScans);

[~,labelIndex] = ismember(scanID, labeledScans); % match data to labels

labelA = metadata{2}; labelA = labelA(labelIndex);
labelB = metadata{3}; labelB = labelB(labelIndex);

% labels where both annotaters agree are '1', rest '0'
matchingLabels = cellfun(@(x,y)(isequal(x,y)), labelA, labelB, 'un', 0); 
matchingLabels = cell2mat(matchingLabels);

labels = cellfun(@(x)(isequal(x,'accept')), labelA); 
labels = labels & matchingLabels;



% -------------------------------------------------------------------------
function [ gif, sweep ] = renderscan( path, callsign, mode, varargin )
% -------------------------------------------------------------------------
%RENDER_SCAN Copy of original RENDERSCAN, but supports multiple modalities
%such as radial velocity ('vr') and spectrum width ('sw') in addition to
%the default relfectivity ('dz').

%radar loading params
DEFAULT_COORDINATES     = 'cartesian'; % 'polar', 'cartesian'
DEFAULT_SWEEPS          = 1;           %a vector of length 1, 2, or 3

%radar alignment params
DEFAULT_AZRES           = 0.5;         %positive real
DEFAULT_RANGERES        = 250;         %positive real
DEFAULT_RANGEMAX        = 37500;       %positive real

%image dimension params
DEFAULT_DIM             = uint16(800); %positive int
DEFAULT_DZLIM           = [-5, 30];    %tuple of reals
DEFAULT_DZMAP           = gray(256);   %colormap

DEFAULT_WRITE_LOCATION  = [];

% parse inputs
parser = inputParser;
addRequired  (parser,'path',                                @(x) exist(x,'file') == 2);
addRequired  (parser,'callsign',                            @(x) validateattributes(x,{'char'},{'numel',4}));
addParamValue(parser,'coordinates', DEFAULT_COORDINATES,    @(x) any(validatestring(x,{'polar','cartesian'})));
addParamValue(parser,'sweeps',      DEFAULT_SWEEPS,         @(x) validateattributes(x,{'numeric'},{'nonempty','positive','integer'}));% && numel(x) <= 3);
addParamValue(parser,'azres',       DEFAULT_AZRES,          @(x) validateattributes(x,{'numeric'},{'>',0}));
addParamValue(parser,'rangeres',    DEFAULT_RANGERES,       @(x) validateattributes(x,{'numeric'},{'>',0}));
addParamValue(parser,'rangemax',    DEFAULT_RANGEMAX,       @(x) validateattributes(x,{'numeric'},{'>',0}));
addParamValue(parser,'dim',         DEFAULT_DIM,            @(x) validateattributes(x,{'numeric'},{'integer','>',0}));
addParamValue(parser,'dzlim',       DEFAULT_DZLIM,          @(x) validateattributes(x,{'numeric'},{'numel',2}));
addParamValue(parser,'dzmap',       DEFAULT_DZMAP,          @(x) iptcheckmap(x,'renderscan','dzmap'));
addParamValue(parser,'outpath',     DEFAULT_WRITE_LOCATION, @(x) isempty(x) || exist(fileparts(x), 'file') == 7);

parse(parser, path, callsign, varargin{:});

path        = parser.Results.path;
callsign    = parser.Results.callsign;
coordinates = parser.Results.coordinates;
sweeps      = parser.Results.sweeps;
azres       = parser.Results.azres;
rangeres    = parser.Results.rangeres;
rangemax    = parser.Results.rangemax;
dim         = parser.Results.dim;
dzlim       = parser.Results.dzlim;
dzmap       = parser.Results.dzmap;
outpath     = parser.Results.outpath;

% read radar

%construct rsl2mat param struct
rsl2mat_params = struct('cartesian', false);%, 'nsweeps', max(sweeps));
sweeps

radar = rsl2mat(path, callsign, rsl2mat_params);

if isequal(mode, 'rh')
    % for corr. coeff, clip values > 2
    for rhs = 1:numel(radar.rh.sweeps)
       x = radar.rh.sweeps(rhs).data; 
       x(x>2) = nan;
       radar.rh.sweeps(rhs).data = x;
    end
end

radar = align_scan_rh(radar, azres, rangeres, rangemax);

% render image

%extract the sweeps as matrices
[data, range, az, ~] = radar2mat(radar, {mode});

%subset the selected sweeps
data = data{1}(:,:,sweeps);


if isequal(mode, 'rh')
    % set NaN to zero correlation for RH
    data(isnan(data)) = 0;
else
    %set NaN to min reflectivity
    data(isnan(data)) = min(dzlim);
end

%allocate gif size
if strcmp(coordinates, 'cartesian')
    %use dim instead
    gif = zeros(dim, dim, size(data,3));
else
    gif = zeros(size(data));
end

%render each sweep into a grayscale gif
for isweep = 1:numel(sweeps)
    sweep = data(:,:,isweep);
    if strcmp(coordinates, 'cartesian')
        %convert to cartesian coordinates
        sweep = mat2cart(sweep, az, range, dim, max(range), 'nearest');
    end
    
    % real-valued matrix to indexed image
    gif(:,:,isweep) = mat2ind(sweep, dzlim, dzmap);
end

%set NaNs in the gif to 0
gif(isnan(gif)) = 0;
gif = uint8(gif);


% for corr. coeff. (RH) set bounds on values to lie in [0, 1.05];
sweep(isnan(sweep)) = 0;

if isequal(mode, 'rh')
    sweep(sweep<0 | sweep>1.05) = 0;
end


%write image to file (if an outpath exists)
if ~isempty(outpath)
	imwrite(gif, outpath);
end



