function run_saliency_thresh()

% Function to debug a trial run of getting "rain" saliency map and
% thresholding it
%   > .95 quantile to get fg mask
%   < .20 quantile to get bg mask

imdb = load('/scratch3/arunirc/radar_project/cnn-radar-scan/deep-face/data/v2-full/radar-sweeps-seed-01/imdb/imdb-seed-1.mat');
net = load('/scratch3/arunirc/radar_project/cnn-radar-scan/deep-face/data/models/ft-sweeps-full-vgg-m.mat');
numSample = 10;
outDir = ...
'/scratch3/arunirc/radar_project/cnn-radar-scan/deep-face/data/v2-full/radar-sweeps-seed-01/vis-segment/saliency-thresh-v4';

rng(0);
setup;
vl_xmkdir(outDir);

switch(net.layers{end}.type)
    case 'softmax'
        net.layers{end} = struct('type', 'softmaxloss') ;
    case 'sofmaxloss'
        % already softmaxloss, do nothing.
    otherwise 
        error('Network final layer needs to be softmax loss');
end
res = [];
rainImgPos = find(imdb.images.label ==2);
rainImgPosSample = rainImgPos(randperm(length(rainImgPos), numSample));

for i = 1:numSample
    
    img = imread(fullfile(imdb.imageDir, ...
                    imdb.images.name{rainImgPosSample(i)}));
    imgOrig = img;
    img = imresize(img, net.normalization.imageSize(1:2), 'bicubic') ;
    img = single(img);
    img = bsxfun(@minus, img, net.normalization.averageImage) ;
    labels = 2;

    % back-prop
    one = single(1) ;
    net.layers{end}.class = labels ;
    res = vl_simplenn(net, img, one, res, ...
      'conserveMemory', true, ...
      'sync', true) ;

    % saliency map
    salImg = abs(squeeze(gather(res(1).dzdx)));
    % salImg = max(salImg, [], 3);
    salImg = sum(salImg.^2, 3).^0.5;

    % distribution of saliency values
    [counts,edges] = histcounts(salImg(:));
    upperThresh = quantile(salImg(:),0.98) ; % 95% quantile
    lowerThresh = quantile(salImg(:),0.20) ; % 20% quantile

    % thresholded image
    upperThreshImg = zeros(size(salImg), 'single');
    lowerThreshImg = zeros(size(salImg), 'single');
    upperThreshImg(salImg > upperThresh) = 1;
    lowerThreshImg(salImg < lowerThresh) = 1;

    % plot figures
    figH = figure(1);
    subplot(1,4,1); 
    imshow(imgOrig);
    title('Radar image');

    subplot(1,4,2);
    imagesc(salImg(:,:,1)); colormap hot; axis square; axis off;
    title('Saliency');

%     
%     subplot(1,4,3);
%     imagesc(lowerThreshImg); colormap hot; axis square; axis off;
%     title('Low. Thresh.');


    subplot(1,4,4);
    imagesc(upperThreshImg); colormap hot; axis square; axis off;
    title('Up. Thresh.');

    set(figH,'Units','normalized');
    set(figH,'Position',[0 0 1 1]);
    
    plotName = sprintf('%d.png', i);
    print(fullfile(outDir,plotName),'-dpng');
    [~,b,c] = fileparts(imdb.images.name{rainImgPosSample(i)});
    imwrite(imgOrig, fullfile(outDir,[b c]));
    imwrite(imgOrig(:,:,1), fullfile(outDir,[b '-1' c]));
    imwrite(imgOrig(:,:,2), fullfile(outDir,[b '-2' c]));
    imwrite(imgOrig(:,:,3), fullfile(outDir,[b '-3' c]));

end
