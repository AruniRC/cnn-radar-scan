% function run_gulf_test()

%% Set paths and data reads
setup;
dataDir = '/scratch3/arunirc/radar_project/radar_data/gulf-coast-data';
labelFileName = 'labels_collated.csv'; % file exists in <dataDir>

% get IMDB
if exist(fullfile('data', 'gulf', 'imdb.mat'))
    imdb = load(fullfile('data', 'gulf', 'imdb.mat'));
else
    imdb = radar_2_get_database(dataDir, labelFileName);
    save(fullfile('data', 'gulf', 'imdb.mat'), '-struct', 'imdb');
end

% load all images 
imArr = cell(1, length(imdb.images.name));
for i = 1:length(imdb.images.name) 
    fprintf('\nLoading image: %d/%d', i, length(imdb.images.name) );
    imArr{i} = imread(fullfile(imdb.imageDir, imdb.images.name{i}));
end
   
   
% load network (assumed you have downloaded to `data/models` directory)
layer = 35;
net = load(fullfile('data', 'models', 'ft-sweeps-full-vgg-vd.mat'));
net.layers = net.layers(1:35); % chop off upper layers
net = vl_simplenn_move(net, 'gpu');

% load pre-trained SVM classifier (modify path as necessary)
svmPath = fullfile('data', 'models', 'svm-rcnnvdft.mat');
svm = load(svmPath);
  

%% Extract feature and evaluate NE pre-trained model on images
%   This is taking a long, long time on CPU.
%   TODO - make batches for better convolution performance

if ~exist(fullfile('data', 'gulf', 'gulf_test_res.mat'))
    score = cell(1,numel(imArr));  
    code = cell(1,numel(imArr));   
    for i = 1:numel(imArr)

        fprintf('\nExtracting features: image: %d/%d', i, length(imArr) );

        im = imArr{i};

        % convert to single, resize to network input size
        im = imresize(single(im), [net.normalization.imageSize(1), ...
                            net.normalization.imageSize(2)], 'bicubic') ;

        % mean subtraction
        im = bsxfun(@minus, im, net.normalization.averageImage) ;

        im = gpuArray(im); % use GPU

        % feature extraction
        res = [];
        res = vl_simplenn(net, im, ...
                        [], res, ...
                        'conserveMemory', true, ...
                        'sync', true) ;
        code_ = squeeze(gather(res(end).x)) ;
        code_ = bsxfun(@times, 1./sqrt(sum(code_.^2)), code_) ;

        % linear SVM score
        score{i} = svm.w'*code_ + svm.b ;

        code{i} = code_ ;
    end

    % save codes
    score = cell2mat(score);
    code = cell2mat(code); 
    save(fullfile('data', 'gulf', 'gulf_test_res.mat'), 'code', 'score', '-v7.3');
else
    load(fullfile('data', 'gulf', 'gulf_test_res.mat'));
end

%% SVM results - pre-trained SVM
%   SVM is trained with "rain" as positive class.
%   So score is negative for image with no "rain".
%   Prediction is -1 for "no-train", +1 for "rain".

% splits
train = imdb.images.set==1;
val = imdb.images.set==2;
test = imdb.images.set==3;

% ROC: estimate optimal threshold using train+val
[tpr, tnr, info] = vl_roc(gt(train|val), score(train|val));
fprintf('\nAccuracy at EER: %f', 1 - info.eer);
fprintf('\nEER threshold: %f\n', info.eerThreshold);

% Test accuracy
pred = -ones(size(score));
pred(score > info.eerThreshold) = 1; 
gt = imdb.images.label;

if ~isequal(size(gt), size(pred))
    pred = pred';
    assert(isequal(size(gt), size(pred)));
end
accu.test = mean(pred(test) == gt(test));
fprintf('\nTest: pre-trained SVM: acc: %f', accu.test);


%% Retrain SVM on new Gulf data

% SVM training - linear SVM, SGD.
y = gt;
n = length(gt);
C = 1;
[w,b] = vl_svmtrain(code(:,train|val), gt(train|val), 1/(n* C), ...
        'epsilon', 0.001, 'verbose', 'biasMultiplier', 1, ...
        'maxNumIterations', n * 200) ;    
    
scores = w'*code + b;
pred = 2*(scores>0) - 1; 

% Test accuracy
if ~isequal(size(gt), size(pred))
    pred = pred';
    assert(isequal(size(gt), size(pred)));
end
accu.test = mean(pred(test) == gt(test));
fprintf('\nTest: re-trained SVM: acc: %f', accu.test);


