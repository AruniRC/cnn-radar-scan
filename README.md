# Detecting rain in radar imagery with deep CNNs

## Base code description

The repository contains code using VLFEAT and MATCONVNET (v1.0-beta9) to:

+ Train CNNs models from scratch or fine-tune on datasets
+ Extract a variety of CNNs features including:
	+ R-CNN : features from CNNs at various layers
	+ D-CNN : CNN filterbanks with Fisher vector pooling
+ Run experiments on radar datasets.
+ Run fine-tuning on the networks.

The two M-files, `run_experiments` and `run_experiments_train`, are the 
starting points for running the CNN model and training it, respectively.

The `setup.m` file needs to change to reflect the paths to compiled libraries 
of **MatConvNet** (beta 6-9 supported) and **VLFeat**. The **WSRLIB** library is required 
for the radar experiments.


## Installing

### Checkout the repository to your local directory 
    
    git clone git@bitbucket.org:AruniRC/cnn-radar-scan.git
    git submodule init
    git submodule update

The code uses three submodules: [vlfeat](http://www.vlfeat.org/), [matconvnet](https://github.com/vlfeat/matconvnet/tree/v1.0-beta9) and matlab-helpers. 
The first two need to be compiled seperately.

### Recommended installation:
You can go to their websites, download the libraries and install them separately.
In that case you need to modify the `setup.m` file to reflect the install paths (recommended).

+ [vlfeat](http://www.vlfeat.org/)
+ [matconvnet](https://github.com/vlfeat/matconvnet/tree/v1.0-beta9)
+ [wsrlib](https://bitbucket.org/dsheldon/wsrlib)
    * Change paths in `src/read-db/radar_get_database.m`  for WSRLIB

Otherwise you can follow these steps to compile them:

### Compiling vlfeat

    cd vlfeat
    make MEX=/exp/comm/matlab-R2014b/bin/mex

### Compiling matconvnet

Checkout a stable release of the matconvnet. On my linux machines I found the v1.0-beta6 release to be stable. You can do this by:
    
    cd matconvnet
    git fetch --tags
    git checkout tags/v1.0-beta6

Edit the Makefile to reflect the paths of CUDA (GPU only) and MATLAB. For example on my linux machine I set the following. 

    ARCH ?= glnxa64
    MATLABROOT ?= /exp/comm/matlab 
    CUDAROOT ?= /usr/local/cuda-6.5

You may have to install libjpeg-dev to enable fast JPEG read support. On an ubuntu machines this is easily done by

    sudo apt-get install libjpeg-dev
    
I compiled the code using the following flags:
    
    make ENABLE_GPU=y ENABLE_IMREADJPEG=y





## Run example radar CNN code 

The `run_single_image.m` file shows how to evaluate the model on radar images.
Two sample rendered radar images are provided as samples. The file contains
details on where to download the trained CNN and SVM models. It depends only 
on having MatConvNet beta-06 (uptil beta-9 compatible) installed and does  
not call other code in this repo.


## Render images from radar data

The rendered images can be computed from the *.gz* radar data files by using 
the following code: 


    setup;
    addpath(genpath(fullfile(RADAR_PATH, 'wsrlib')));
    fileName = 'data/sample/KBGM20100802_002000_V03.gz';
    stationName = 'KBGM';

    img = renderscan(fileName, stationName, 'dz', 'sweeps',  [1 2 3]);

    imshow(img)


This depends on the function `renderscan()` and the external **WSRLib** 
library.

## Synthesis project

This work is for my synthesis project at UMass Amherst, advised by Daniel Sheldon [computational ecology], Erik Learned-Miller [vis] and Subhransu Maji [vis].





	

