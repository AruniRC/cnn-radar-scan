
% RUN_GULF_TEST
%

%% Set paths and load data
setup;
rng(0);

imdb = load(fullfile('data', 'gulf', 'imdb.mat'));

train = imdb.images.set==1;
val = imdb.images.set==2;
test = imdb.images.set==3;
gt = imdb.images.label;

scores = w'*code + b;
pred = 2*(scores>0) - 1; 
if ~isequal(size(gt), size(pred))
    pred = pred';
    assert(isequal(size(gt), size(pred)));
end

%%

fid = fopen('radar-svm.csv', 'w');

s = {};
test_images = imdb.images.name(test);
test_labels = imdb.images.label(test);
test_pred = pred(test);

s{end+1} = sprintf('Filename,Label,Prediction\n');
for i = 1:numel(test_images)
   
   s{end+1} = sprintf('%s,%d,%d\n', ...
                        test_images{i}, ...
                        test_labels(i), ...
                        test_pred(i));    
end
str = cat(2, s{:}) ;
fprintf(fid, '%s', str) ;
fclose(fid) ;

