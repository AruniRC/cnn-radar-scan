run vlfeat/toolbox/vl_setup
run matconvnet/matlab/vl_setupnn
addpath matconvnet/examples/
addpath matlab-helpers
clear mex ;

RADAR_PATH = '/scratch3/arunirc/radar_project/';

addpath(genpath('src'));

%{
vl_compilenn('enableGpu', true, 'cudaMethod', 'nvcc', ...
'cudaRoot', '/usr/local/cuda', ...
'enableImreadJpeg', true, 'Verbose', 1) ;
%}
