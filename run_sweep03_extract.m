% The sweep-3 images for the full radar dataset have several corrupted
% files, which has reduced the total number of images when compared to the
% dataset of sweep-1-2-3 images. This scripts loads in each of the
% sweeps-1-2-3 images as RGB images and saves the 3rd channel, which is
% actually the 3rd-elevation sweep, as the sweep-3 images.
imdb = load('/scratch3/arunirc/radar_project/cnn-radar-scan/deep-face/data/v2-full/radar-sweeps-seed-01/imdb/imdb-seed-1.mat');

parfor i = 1:numel(imdb.images.name)
    
    fprintf('%d/%d\n', i, numel(imdb.images.name));
    
    img = imread(fullfile(imdb.imageDir, imdb.images.name{i})); 
    imSweep = img(:,:,3);
    imgName = [imdb.images.name{i}(1:end-10) '_3.jpg'];
    imwrite(imSweep, fullfile(imdb.imageDir, imgName)); 
    
end