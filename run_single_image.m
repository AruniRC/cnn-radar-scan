% function run_single_image()
% RUN_SINGLE_IMAGE 
%
%   Example usage (self-contained) for evaluating the model on individual 
%   images, does not use GPU.
%
%   Code dependencies: none
%   Software dependencies: MatConvNet beta-09
%   Data dependencies: downloadable samples images, SVM and CNN models.
%       http://fisher.cs.umass.edu/~arunirc/radar_project/models/
%       Download into data/models folder

% load sample images
imArr = { imread('data/sample/scan_rain.jpg'), ...
       imread('data/sample/scan_no-rain.jpg') };
   
% load network (assumed you have downloaded to `data/models` directory)
layer = 35;
net = load(fullfile('data', 'models', 'ft-sweeps-full-vgg-vd.mat'));
net.layers = net.layers(1:35); % chop off upper layers

% load trained SVM classifiimdb = radar_2_get_database(dataDir, labelFileName)er (modify path as necessary)
svmPath = fullfile('data', 'models', 'svm-rcnnvdft.mat');
svm = load(svmPath);
  

%% evaluate model on sample images
score = cell(1,numel(imArr));   
for i = 1:numel(imArr)
    
    im = imArr{i};
    
    % convert to single, resize to network input size
    im = imresize(single(im), [net.normalization.imageSize(1), ...
                        net.normalization.imageSize(2)], 'bicubic') ;
                    
    % mean subtraction
    im = bsxfun(@minus, im, net.normalization.averageImage) ;
    
    
    % feature extraction
    res = [];
    res = vl_simplenn(net, im, ...
                    [], res, ...
                    'conserveMemory', true, ...
                    'sync', true) ;
    code_ = squeeze(gather(res(end).x)) ;
    code_ = bsxfun(@times, 1./sqrt(sum(code_.^2)), code_) ;
    
    % linear SVM score
    score{i} = svm.w'*code_ + svm.b ;
end

%% Visualise results
%   SVM is trained with "rain" as positive class.
%   So score is negative for image with no rain.
figure;
subplot(1,2,1); imshow(imArr{1}); xlabel(['rain score=' num2str(score{1})]);
subplot(1,2,2); imshow(imArr{2}); xlabel(['rain score=' num2str(score{2})]);    
    
    

